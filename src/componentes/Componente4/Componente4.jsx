import React from 'react'
import './stylec4.css'
import foto1 from '../../assets/img/img3.png'

function componente4() {
  return (
  <div className="card">
    <div className="title"><p>JUEGOS</p></div>
    <div className="descripcion"><p>Me gustan mucho los videojuegos y mi favorito es CSGO</p></div>
    <div className="image"><img className="img4" src={foto1} alt="" /></div>
  </div>
  )
}

export default componente4