import Componente1 from "./componentes/Componente1/Componente1"
import Componente2 from "./componentes/Componente2/Componente2"
import Componente3 from "./componentes/Componente3/Componente3"
import Componente4 from "./componentes/Componente4/Componente4"
import Componente5 from "./componentes/Componente5/Componente5"

function App() {


  return (

<div className="contenedor">

  <div className="cont1"><Componente1/></div>
  <div className="cont2"><Componente2/></div>
  <div className="cont3"><Componente3/></div>
  <div className="cont4"><Componente4/></div>
  <div className="cont5"><Componente5/></div>

</div>
  )
}

export default App




